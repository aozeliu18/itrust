package edu.ncsu.csc.itrust.selenium;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * Custom implementation of an HtmlUnitDriver
 * that does not report js errors.
 */
public class Driver extends HtmlUnitDriver {
	/**
	 * Construct a new driver and disable script error reporting.
	 */
	public Driver(String testName) {
		super();
		WebClient webClient = getWebClient();
		webClient.setThrowExceptionOnScriptError(false);
		webClient.addRequestHeader("job_id", System.getenv().getOrDefault("TASK_ID", "anonymous-task"));
		webClient.addRequestHeader("spec_file_name", testName);
	}

	public Driver(String testName, BrowserVersion version) {
		super(version);
		WebClient webClient = getWebClient();
		webClient.setThrowExceptionOnScriptError(false);
		webClient.addRequestHeader("job_id", System.getenv().getOrDefault("TASK_ID", "anonymous-task"));
		webClient.addRequestHeader("spec_file_name", testName);
	}
}
